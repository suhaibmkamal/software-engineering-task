<?php

use yii\db\Migration;

/**
 * Class m181118_211924_post
 */
class m181118_211924_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('post',[

            'post_id'=> $this->primaryKey(),
            'title'=> $this->string(),
            'description'=> $this->text(),
            'category_id'=> $this->integer()->notNull(),
            'sub_category_id'=> $this->integer()->notNull()

        ],$tableOptions);

        $this->createIndex(
            'idx-post-category_id',
            'post',
            'category_id'
        );

        // add foreign key for table `post`
        $this->addForeignKey(
            'fk-post-category_id',
            'post',
            'category_id',
            'category',
            'category_id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-post-sub_category_id',
            'post',
            'sub_category_id'
        );

        // add foreign key for table `post`
        $this->addForeignKey(
            'fk-post-sub_category_id',
            'post',
            'sub_category_id',
            'sub_category',
            'sub_category_id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-post-category_id',
            'post'
        );

        // drops index for column `post_id`
        $this->dropIndex(
            'idx-post-category_id',
            'post'
        );


        $this->dropForeignKey(
            'fk-post-sub_category_id',
            'post'
        );

        // drops index for column `post_id`
        $this->dropIndex(
            'idx-post-sub_category_id',
            'post'
        );

        $this->dropTable('post');
        echo "m181118_211924_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181118_211924_post cannot be reverted.\n";

        return false;
    }
    */
}
