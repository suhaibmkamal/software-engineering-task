<?php

use yii\db\Migration;

/**
 * Class m181118_211855_category
 */
class m181118_211855_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('category',[

            'category_id'=> $this->primaryKey(),
            'category_name'=> $this->string()
        ],$tableOptions);

        $this->insert('category', [
            'category_name' => 'Cars'
            
        ]);

        $this->insert('category', [
            'category_name' => 'Houses'
            
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->delete('category', ['id' => 1]);
        $this->delete('category', ['id' => 2]);
        $this->dropTable('category');
        
        echo "m181118_211855_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181118_211855_category cannot be reverted.\n";

        return false;
    }
    */
}
