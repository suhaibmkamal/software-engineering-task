<?php

use yii\db\Migration;

/**
 * Class m181118_211915_sub_category
 */
class m181118_211915_sub_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('sub_category',[

            'sub_category_id'=> $this->primaryKey(),
            'sub_category_name'=> $this->string(),
            'category_id'=> $this->integer()->notNull()
        ],$tableOptions);

        $this->createIndex(
            'idx-sub_category-category_id',
            'sub_category',
            'category_id'
        );

        // add foreign key for table `post`
        $this->addForeignKey(
            'fk-sub_category-category_id',
            'sub_category',
            'category_id',
            'category',
            'category_id',
            'CASCADE'
        );

        $this->insert('sub_category', [
            'sub_category_name' => 'Cars for sale',
            'category_id' => 1
            
        ]);

        $this->insert('sub_category', [
            'sub_category_name' => 'Cars for rent',
            'category_id' => 1
        ]);

        $this->insert('sub_category', [
            'sub_category_name' => 'Houses for sale',
            'category_id' => 2
            
        ]);

        $this->insert('sub_category', [
            'sub_category_name' => 'Houses for rent',
            'category_id' => 2
            
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->delete('sub_category', ['id' => 1]);
        $this->delete('sub_category', ['id' => 2]);
        $this->delete('sub_category', ['id' => 3]);
        $this->delete('sub_category', ['id' => 4]);

        $this->dropForeignKey(
            'fk-sub_category-category_id',
            'sub_category'
        );

        // drops index for column `post_id`
        $this->dropIndex(
            'idx-sub_category-category_id',
            'sub_category'
        );

        $this->dropTable('sub_category');
        echo "m181118_211915_sub_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181118_211915_sub_category cannot be reverted.\n";

        return false;
    }
    */
}
