<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\Category;
use backend\models\SubCategory;

/* @var $this yii\web\View */
/* @var $model backend\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Category::find()->all(),'category_id','category_name'),
    ['prompt'=>'Select category',
    'onchange'=>'
      $.post( "index.php?r=post/lists&id='.'"+$(this).val(), function( data ) {
        $( "select#post-sub_category_id" ).html( data );
      });
  '])?>

    <?= $form->field($model, 'sub_category_id')->dropDownList(ArrayHelper::map(
        SubCategory::findAll('category_id'==$model->category_id),
        'sub_category_id',
        'sub_category_name'
    ),
    ['prompt'=>'Select sub category'] )?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
